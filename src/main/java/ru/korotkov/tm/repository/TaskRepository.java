package ru.korotkov.tm.repository;

import ru.korotkov.tm.entity.Task;

import java.util.*;

public class TaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    private final HashMap<String, HashSet<Task>> mapTasks = new HashMap<>();

    /**
     * Create task by name
     *
     * @param name - name of task
     * @param userId
     * @return created task
     */
    public Task create(final String name, final Long userId) {
        final Task task = new Task(name, userId);
        tasks.add(task);
        addHashMap(task);
        return task;
    }

    /**
     * Create task by name and description
     *
     * @param name        - name of task
     * @param description - description of task
     * @param userId
     * @return created task
     */
    public Task create(final String name, final String description, final Long userId) {
        final Task task = create(name, userId);
        task.setDescription(description);
        return task;
    }

    /**
     * Add task to hashmap
     *
     * @param task - task
     */
    private void addHashMap(final Task task) {
        final String name = task.getName();
        HashSet<Task> tasks = mapTasks.get(name);
        if (tasks == null) {
            tasks = new HashSet<>();
            tasks.add(task);
            mapTasks.put(name, tasks);
        } else {
            tasks.add(task);
        }
    }

    /**
     * Remove task from hashmap
     *
     * @param task - task
     */
    private void removeFromHashMap(final Task task) {
        HashSet<Task> tasks = mapTasks.get(task.getName());
        if (tasks != null) {
            tasks.remove(task);
        }
    }

    /**
     * Clear tasks
     *
     * @param userId
     */
    public void clear(final Long userId) {
        final List<Task> removeTasks = findAll(userId);
        tasks.removeAll(removeTasks);
        for (final Task task : removeTasks) {
            removeFromHashMap(task);
        }
    }

    /**
     * Get size of repository
     *
     * @param userId
     */
    public int getSize(final Long userId) {
        return findAll(userId).size();
    }

    /**
     * Find task by index
     *
     * @param index index of task
     * @param userId
     * @return task or null
     */
    public Task findByIndex(final int index, final Long userId) {
        Task task = findAll(userId).get(index);
        if (task == null) {
            return null;
        }
        if (!task.getUserId().equals(userId)) {
            return null;
        }
        return task;
    }

    /**
     * Find first task by name
     *
     * @param name name of task
     * @param userId
     * @return task or null
     */
    public Task findByName(final String name, final Long userId) {
        HashSet<Task> tasks = mapTasks.get(name);
        if (tasks == null) {
            return null;
        }
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) {
                return task;
            }
        }
        return null;
    }

    /**
     * Find task by id
     *
     * @param id id of task
     * @param userId
     * @return task or null
     */
    public Task findById(final Long id, final Long userId) {
        Task result = null;
        for (final Task task : tasks) {
            if (id.equals(task.getId())) {
                result = task;
                break;
            }
        }
        if (result != null && !result.getUserId().equals(userId)) {
            return null;
        }
        return result;
    }

    /**
     * Remove task from structures
     *
     * @param task - task
     */
    private void remove(final Task task) {
        final String name = task.getName();
        tasks.remove(task);
        HashSet<Task> setTasks = mapTasks.get(name);
        if (setTasks != null) {
            setTasks.remove(task);
        }
    }

    /**
     * Remove task by index
     *
     * @param index index of task
     * @param userId
     * @return task or null
     */
    public Task removeByIndex(final int index, final Long userId) {
        Task task = findByIndex(index, userId);
        if (task != null) {
            remove(task);
        }
        return task;
    }

    /**
     * Remove task by name
     *
     * @param name name of task
     * @param userId
     * @return task or null
     */
    public Task removeByName(final String name, final Long userId) {
        Task task = findByName(name, userId);
        if (task != null) {
            remove(task);
        }
        return task;
    }

    /**
     * Remove task by id
     *
     * @param id id of task
     * @param userId
     * @return task or null
     */
    public Task removeById(final Long id, final Long userId) {
        Task task = findById(id, userId);
        if (task != null) {
            remove(task);
        }
        return task;
    }

    /**
     * Change name of task
     *
     * @param task task
     * @param name new name
     */
    private void changeNameOfTask(final Task task, final String name) {
        removeFromHashMap(task);
        task.setName(name);
        addHashMap(task);
    }

    /**
     * Update task by index
     *
     * @param index index of task
     * @param name name of task
     * @param userId
     * @return task or null
     */
    public Task updateByIndex(final int index, final String name, final Long userId) {
        Task task = findByIndex(index, userId);
        if (task == null) {
            return null;
        }
        changeNameOfTask(task, name);
        return task;
    }

    /**
     * Update task by index
     *
     * @param index index of task
     * @param name name of task
     * @param description description of task
     * @param userId
     * @return task or null
     */
    public Task updateByIndex(final int index, final String name, final String description, final Long userId) {
        Task task = updateByIndex(index, name, userId);
        if (task != null) {
            task.setDescription(description);
        }
        return task;
    }

    /**
     * Update task by id
     *
     * @param id id of project
     * @param name name of project
     * @param userId
     * @return project or null
     */
    public Task updateById(final Long id, final String name, final Long userId) {
        Task task = findById(id, userId);
        if (task == null) {
            return null;
        }
        changeNameOfTask(task, name);
        return task;
    }

    /**
     * Update task by id
     *
     * @param id id of project
     * @param name name of project
     * @param description description of project
     * @param userId
     * @return project or null
     */
    public Task updateById(final Long id, final String name, final String description, final Long userId) {
        Task task = updateById(id, name, userId);
        if (task != null) {
            task.setDescription(description);
        }
        return task;
    }

    /**
     * Return tasks
     *
     * @param userId
     */
    public List<Task> findAll(final Long userId) {
        ArrayList<Task> result = new ArrayList<>();
        for (final Task task: tasks) {
            if (task.getUserId().equals(userId)) {
                result.add(task);
            }
        }
        return result;
    }

    /**
     * Return tasks by projectId
     *
     * @param projectId id of project
     * @param userId
     */
    public List<Task> findTaskByProjectId(final Long projectId, final Long userId) {
        List<Task> result = new ArrayList<>();
        for (final Task task: findAll(userId)) {
            if (task.getProjectId().equals(projectId)) {
                result.add(task);
            }
        }
        return result;
    }

}