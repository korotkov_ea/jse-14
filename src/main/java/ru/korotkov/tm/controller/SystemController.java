package ru.korotkov.tm.controller;

import ru.korotkov.tm.service.SessionService;

public class SystemController extends AbstractController {

    private final SessionService sessionService;

    public SystemController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    /**
     * Get userId
     *
     * @return userId - id of user
     */
    public Long getUserId() {
        return sessionService.getUserId();
    }

    /**
     * History of commands
     */
    public void historyCommand() {
        int index = 1;
        for (final String command : sessionService.findAllCommand()) {
            System.out.println("INDEX: " + index++ + " COMMAND: " + command);
        }
    }

    /**
     * Add command to history
     *
     * @param command - command
     */
    public void addCommand(final String command) {
        sessionService.addCommand(command);
    }

    /**
     * Start new session
     *
     * @param userId - id of user
     */
    public void startSession(final Long userId) {
        sessionService.startSession(userId);
    }

    /**
     * Close session
     */
    public void closeSession() {
        sessionService.closeSession();
    }

    /**
     * Welcome information
     */
    public void displayWelcome() {
        System.out.println(bundle.getString("welcome"));
    }

    /**
     * Display version of program
     */
    public void displayVersion() {
        System.out.println(bundle.getString("version"));
    }

    /**
     * Display information about of program
     */
    public void displayAbout() {
        System.out.println(bundle.getString("about"));
    }

    /**
     * Display help
     */
    public void displayHelp() {
        System.out.println(bundle.getString("help"));
    }

    /**
     * Display stub
     */
    public void displayStub(final String line) {
        System.out.println(String.format(bundle.getString("stub"), line));
    }

}
