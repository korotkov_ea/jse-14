package ru.korotkov.tm.controller;

import java.util.ResourceBundle;

public abstract class AbstractController {

    protected final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

}
